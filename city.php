<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- Style -->
<link href="css/style.css" rel="stylesheet" type="text/css"/>
<?php
require_once("functions.php");

$cityCode = $_GET["city"];

$file = file_get_contents(createUrl("weatherappmh", "69a06a4238972db73c13d4ce1fcda89b", $cityCode));

$values = parseJSON($file);
if ($values["name"] != NULL):
	$cData = getHeadimg($cityCode);
	$curFile = $file;
	?>
	<!Doctype html>
	<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta charset="utf-8">
		<title>Wetter in <?php echo $values["name"]; ?></title>

		<!-- Favicon -->
		<link rel="icon" href="img/symbole/schwarz/2.png"/>

		<!-- Schrift -->
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,400italic,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>



		<style type="text/css">
		header.backgroundimg.row{
			background-image: url(<?php global $cData; echo $cData["img"]; ?>);
		}
		</style>
	</head>

	<body>
		<div class="container-fluid header">
			<header class="backgroundimg row">

				<nav class="navbar navbar-default custom">
					<div class="container-fluid">
						<div class="navbar-header">
							<a class="navbar-brand mobileversion" href="index.html"><strong>WETTERAUSSICHTEN FÜR HEUTE</strong></a>
						</div>
					</nav>

					<div class="background row">

						<div class="col-md-4 col-sm-4 col-xs-4">
							<p class="aktuellgrad"><strong><?php echo $values["avgTemp"]; ?>°</strong></p>
						</div>

						<div class="col-md-2 col-sm-2 col-xs-2 desktop1">
							<img src="<?php echo typeToSymbol($values['weather'], "weiss") ?>" class="wetteraktuell" title="<?php echo $values['weathertxt']; ?>" />
						</div>

						<div class="col-md-6 col-sm-6 col-xs-6">
							<p class="city"><strong><?php echo $values["name"]; ?></strong></p>
							<p class="datum">
								<?php
								$d = translateDay(date("l"));
								$m = translateMonth(date("F"));
								echo $d . ", " . date("j") . ". " . $m;
								?>
							</p>
						</div>
					</div>
				</header>
			</div>

			<div class="container-fluid">
				<main>
					<div class="Stats row">

						<div class="col-md-4 col-sm-4 col-xs-4">
							<img src="img/symbole/uhr.png" class="symbol" title="Uhrzeit"><p class="infos"></br><strong><?php echo date("G:i"); ?> UHR</strong></p>
						</div>

						<div class="col-md-4 col-sm-4 col-xs-4">
							<img src="img/symbole/schirm.png" class="symbol" title="Regenwahrscheinlichkeit"><p class="infos"></br><strong><?php echo $values["rain"]; ?>%</strong></p>
						</div>

						<div class="col-md-4 col-sm-4 col-xs-4">
							<img src="img/symbole/sun.png" class="symbol" title="Sonnenauf- und untergang"><p class="infos"></br><strong>
								<?php
								$now = time();
								$gmt_offset = 1;
								$zenith = 50/60;
								$zenith = $zenith + 90;
								$sunset = date_sunset($now, SUNFUNCS_RET_TIMESTAMP, $cData["b"], $cData["l"], $zenith, $gmt_offset);
								$sunrise= date_sunrise($now, SUNFUNCS_RET_TIMESTAMP, $cData["b"], $cData["l"], $zenith, $gmt_offset);

								echo date("H:i",$sunrise)."/";
								echo date("H:i",$sunset);
								?></strong></p>
							</div>
						</div>
						<?php
							$parsed = array(
								"6" => parseJSONtime($curFile, '06:00'),
								"11" => parseJSONtime($curFile, '11:00'),
								"17" => parseJSONtime($curFile, '17:00'),
								"23" => parseJSONtime($curFile, '23:00')
							);
						?>

						<div class="aussichten row">
						<?php foreach ($parsed as $k => $v): ?>
							<?php
								$intk = intval($k);
								if ($intk == 6) $nk = $intk + 5;
								else if ($intk == 23) $nk = 6;
								else $nk = $intk + 6;
							?>
							<div class="col-md-3 col-sm-3 col-xs-3">
								<p class="Do"><?php echo $k . "-" . $nk; ?></p>
								<img src="<?php  echo typeToSymbol($v['weather'], 'schwarz'); ?>" title="<?php echo $v['weathertxt']; ?>" class="icon" />
								<p class="temp"><?php echo $v["minTemp"]; ?>°/<?php echo $v["maxTemp"]; ?>°</p>
							</div>

						<?php endforeach; ?>

							<div class="col-md-12 col-sm-12 col-xs-12 back">
								<a href="index.html" class="back"><img src="img/symbole/back.png" class="back" />Zurück zur Übersicht</a>
							</div>
						</div>
					</div>
				</main>

				<footer>
					<p><small>Die aktuelle Temperatur wird um 6, 11, 17 und 23 Uhr aktualisiert.</small></p>
					<img src="img/symbole/info.png" class="symbol" />  <a href="infos.html"><small>Weitere Informationen |</small></a>
					<img src="img/symbole/powered.png" class="symbol"/>
					<?php
					echo "<small><a href='" . $values["credLink"] . "' target='_blank'>" . $values["credText"] . "</a></small>";
					?>
					<p><small>Bilder: <a href="http://myswitzerland.com" target="_blank">myswitzerland.com</a>, Icons: <a href="http://flaticon.com" target="_blank">flaticon.com</a></small></p>
				</footer>

			<?php endif ?>
			<?php global $values; if ($values["name"] == NULL): ?>
				<div class="return value error">
					<p class="error-heading">Ups!</p>
					<p class="error-text">
						Anscheinend gibt es ein Problem.<br>
						<b>Mögliche Ursachen:</b>
						<ul>
							<li>Keine Internetverbindung</li>
							<li>Ungültige URL</li>
							<li>Die Website des Datenlieferants (wetter.com) ist offline</li>
						</ul>
					</p>
				</div>
			<?php endif; ?>
		</body>

		<!-- jQuery-->
		<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

		</html>
