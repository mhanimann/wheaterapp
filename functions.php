<?php
// Create URL and retrieve information about weather-station
function getLocationCode($place){
	$searchUrl = "http://api.wetter.com/location/index";
	$projectName = "weatherappmh";
	$apiKey = "69a06a4238972db73c13d4ce1fcda89b";
	$searchString = $place;

	$checkSum = md5($projectName . $apiKey . $searchString);

	$searchUrl .= "/search/" . $searchString;
	$searchUrl .= "/project/" . $projectName;
	$searchUrl .= "/output/json";
	$searchUrl .= "/cs/" . $checkSum;

	echo $searchUrl;
	echo file_get_contents($searchUrl);

	$completeFile = file_get_contents($searchUrl);
	return $completeFile;
}

// Create URL for retrieving weather-data in JSON-format
function createURL($projName, $key, $city){
	// Set basic stuff
	$forecastUrl = "http://api.wetter.com/forecast/weather";
		$projectName = $projName;
		$apiKey = $key;
		$cityCode = $city;

		// Generate Checksum
		$checkSum = md5($projectName . $apiKey . $cityCode);

		// Create URL
		$forecastUrl .= "/city/" . $cityCode;
		$forecastUrl .= "/output/json";
		$forecastUrl .= "/project/" . $projectName;
		$forecastUrl .= "/cs/" . $checkSum;

		return $forecastUrl;
	}

	// Parse forecast-file and return array of necessairy values
	function parseJSON($file){
		$date = date("Y-m-d");
		$curTimeStr = date("H");
		$curTime = intval($curTimeStr);
		$time = "";
		if ($curTime >= 23 && $curTime < 6){
			$time = "23:00";
		}
		else if ($curTime >= 6 && $curTime < 11){
			$time = "06:00";
		}
		else if ($curTime >= 11 && $curTime < 17){
			$time = "11:00";
		}
		else if ($curTime >= 17 && $curTime < 23){
			$time = "17:00";
		}
		else{
			$time = "undefined";
		}
		$jsonValue = json_decode($file, true);
		if ($time == "undefined"){
			$minTemp = $jsonValue["city"]["forecast"][$date]["tn"];
			$maxTemp = $jsonValue["city"]["forecast"][$date]["tx"];
			$weather = $jsonValue["city"]["forecast"][$date]["w"];
			$rain = $jsonValue["city"]["forecast"][$date]["pc"];
			$weathertxt = $jsonValue["city"]["forecast"][$date]["w_txt"];
		}
		else{

			$curMinTemp = $jsonValue["city"]["forecast"][$date][$time]["tn"];
			$curMaxTemp = $jsonValue["city"]["forecast"][$date][$time]["tx"];
			$curWeather = $jsonValue["city"]["forecast"][$date][$time]["w"];
			$curRain = $jsonValue["city"]["forecast"][$date][$time]["pc"];
			$weathertxt = $jsonValue["city"]["forecast"][$date][$time]["w_txt"];
		}
		$cityName = $jsonValue["city"]["name"];
		$credText = $jsonValue["city"]["credit"]["text"];
		$credLink = $jsonValue["city"]["credit"]["link"];
		$curAvgTemp = round(($curMinTemp + $curMaxTemp) / 2);
		$returnValues = array(
			"name" => $cityName,
			"minTemp" => $curMinTemp,
			"avgTemp" => $curAvgTemp,
			"maxTemp" => $curMaxTemp,
			"rain" => $curRain,
			"weather" => $curWeather,
			"weathertxt" => $weathertxt,
			"credText" => $credText,
			"credLink" => $credLink
		);
		return $returnValues;
	}

	// Parse forecast for information on specific times
	function parseJSONtime($file, $time){
		$date = date("Y-m-d");

		$jsonValue = json_decode($file, true);

		$minTemp = $jsonValue["city"]["forecast"][$date][$time]["tn"];
		$maxTemp = $jsonValue["city"]["forecast"][$date][$time]["tx"];
		$weather = $jsonValue["city"]["forecast"][$date][$time]["w"];
		$rain = $jsonValue["city"]["forecast"][$date][$time]["pc"];
		$weathertxt = $jsonValue["city"]["forecast"][$date][$time]["w_txt"];

		return array(
			"minTemp" => $minTemp,
			"maxTemp" => $maxTemp,
			"weather" => $weather,
			"rain" => $rain,
			"weathertxt" => $weathertxt
		);
	}

	// Show symbol for Weather-types
	function typeToSymbol($type, $color){
		// Sonnig
		if ($type == "0") $symbol = 1;
		// leicht bewölkit
		else if ($type == "1" || $type == "10") $symbol = 2;
		// Wolkig mit Wahrscheinlichkeit auf Fleischbällchen
		else if ($type == "2" || $type == "20") $symbol = 3;
		// Bedeckt
		else if ($type == "3" || $type == "30") $symbol = 4;
		// Nebel
		else if ($type == "4" || $type == "40" || $type == "45" || $type == "48" || $type == "49") $symbol = 15;
		// Sprühregen
		else if ($type == "5" || $type == "50" || $type == "51" || $type == "53" || $type == "55") $symbol = 14;
		// Ein Schiff wird kommen
		else if ($type == "6" || $type == "8" || $type == "60" || $type == "61" || $type == "66" || $type == "80") $symbol = 5;
		else if ($type == "63" || $type == "65" || $type == "81" || $type == "82") $symbol = 18;
		// Schnee
		else if ($type == "7" || $type == "70" || $type == "71" || $type == "73" || $type == "75" || $type == "83" || $type == "84" || $type == "85" || $type == "86") $symbol = 6;
		else if ($type == "68" || $type == "69") $symbol = 17;
		// Gewitter
		else if ($type == "9" || $type == "90" || $type == "95" || $type == "96") $symbol = 7;

		//failsafe
		else $symbol = "fail";

		return "img/symbole/" . $color . "/" . $symbol . ".png";
	}

	// Get the correct image for the current page
	function getHeadimg($cityCode){
		if ($cityCode == "CH0CH0002"){
			$city = "aarau";
			$l = 8.05;
			$b = 47.38;
		}
		else if ($cityCode == "CH0CH0093"){
			$city = "altdorf";
			$l = 8.65;
			$b = 46.88;
		}
		else if ($cityCode == "CH0CH0135"){
			$city = "appenzell";
			$l = 9.41;
			$b = 47.33;
		}
		else if ($cityCode == "CH0CH0260"){
			$city = "basel";
			$l = 7.6;
			$b = 47.56;
		}
		else if ($cityCode == "CH0CH0324"){
			$city = "bern";
			$l = 7.46;
			$b = 46.91;
		}
		else if ($cityCode == "CH0CH0391"){
			$city = "biel";
			$l = 7.24;
			$b = 47.13;
		}
		else if ($cityCode == "CH0CH0825"){
			$city = "chur";
			$l = 9.5;
			$b = 46.85;
		}
		else if ($cityCode == "CH0CH1007"){
			$city = "delemont";
			$l = 7.33;
			$b = 47.36;
		}
		else if ($cityCode == "CH0CH1358"){
			$city = "frauenfeld";
			$l = 8.9;
			$b = 47.55;
		}
		else if ($cityCode == "CH0CH1379"){
			$city = "freiburg";
			$l = 7.15;
			$b = 46.8;
		}
		else if ($cityCode == "CH0CH1504"){
			$city = "glarus";
			$l = 9.06;
			$b = 47.03;
		}
		else if ($cityCode == "CH0CH1437"){
			$city = "genf";
			$l = 6.16;
			$b = 46.2;
		}
		else if ($cityCode == "CH0CH2017"){
			$city = "lachauxdefonds";
			$l = 6.85;
			$b =47.13;
		}
		else if ($cityCode == "CH0CH2112"){
			$city = "lausanne";
			$l = 6.66;
			$b = 46.53;
		}
		else if ($cityCode == "CH0CH2346"){
			$city = "lugano";
			$l = 8.94;
			$b = 46;
		}
		else if ($cityCode == "CH0CH2389"){
			$city = "luzern";
			$l = 8.26;
			$b = 47.08;
		}
		else if ($cityCode == "CH0CH2650"){
			$city = "montreux";
			$l = 6.91;
			$b = 46.43;
		}
		else if ($cityCode == "CH0CH2775001"){
			$city = "neuenburg";
			$l = 6.96;
			$b = 47;
		}
		else if ($cityCode == "CH0CH3500"){
			$city = "sarnen";
			$l = 8.23;
			$b = 46.9;
		}
		else if ($cityCode == "CH0CH3526"){
			$city = "schaffhausen";
			$l = 8.63;
			$b = 47.7;
		}
		else if ($cityCode == "CH0CH3617"){
			$city = "schwyz";
			$l = 8.65;
			$b = 47.01;
		}
		else if ($cityCode == "CH0CH3697"){
			$city = "sion";
			$l = 7.35;
			$b = 46.23;
		}
		else if ($cityCode == "CH0CH3719"){
			$city = "solothurn";
			$l = 7.51;
			$b = 47.23;
		}
		else if ($cityCode == "CH0CH3832"){
			$city = "stans";
			$l = 8.35;
			$b = 46.96;
		}
		else if ($cityCode == "CH0CH3756"){
			$city = "stgallen";
			$l = 9.4;
			$b = 47.46;
		}
		else if ($cityCode == "CH0CH3772"){
			$city = "stmoritz";
			$l = 9.88;
			$b = 46.50;
		}
		else if ($cityCode == "CH0CH3960"){
			$city = "thun";
			$l = 7.61;
			$b = 46.75;
		}
		else if ($cityCode == "CH0CH4408"){
			$city = "winterthur";
			$l = 8.75;
			$b = 47.5;
		}
		else if ($cityCode == "CH0CH4492"){
			$city = "zug";
			$l = 8.52;
			$b = 47.19;
		}
		else if ($cityCode == "CH0CH4503"){
			$city = "zuerich";
			$l = 8.55;
			$b = 47.36;
		}
		else{
			$city = "placehold";
			$l = 13.36;
			$b = 46.48;
		}

		return array(
			"img" => "img/orte/" . $city . ".jpg",
			"l" => $l,
			"b" => $b
		);

	}
	// Translate names of days and months to german
	function translateDay($date){
		if ($date == "Monday") return "Montag";
		else if ($date == "Tuesday") return "Dienstag";
		else if ($date == "Wednesday") return "Mittwoch";
		else if ($date == "Thursday") return "Donnerstag";
		else if ($date == "Friday") return "Freitag";
		else if ($date == "Saturday") return "Samstag";
		else if ($date == "Sunday") return "Sonntag";
		else if ($date == "Mon") return "MO";
		else if ($date == "Tue") return "DI";
		else if ($date == "Wed") return "MI";
		else if ($date == "Thu") return "DO";
		else if ($date == "Fri") return "FR";
		else if ($date == "Sat") return "SA";
		else if ($date == "Sun") return "SO";
		else return "Scheisstag";
	}
	function translateMonth($date){
		if ($date == "January") return "Januar";
		else if ($date == "February") return "Februar";
		else if ($date == "March") return "März";
		else if ($date == "April") return "April";
		else if ($date == "May") return "Mai";
		else if ($date == "June") return "Juni";
		else if ($date == "July") return "Juli";
		else if ($date == "August") return "August";
		else if ($date == "September") return "September";
		else if ($date == "October") return "Oktober";
		else if ($date == "November") return "November";
		else if ($date == "December") return "Dezember";
		else return "Karneval";
	}
	?>
